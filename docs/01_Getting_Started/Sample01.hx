import stipple.Template;

class Sample01 {
	static function main() {

		var output = new Template ().fromString(

			"Hello {{language}}! {{! This is a comment syntax }}"

		).execute({ 

			language: "haxe"

		});

		trace(output);

	}
}
