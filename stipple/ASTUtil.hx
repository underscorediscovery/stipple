package stipple;

using Lambda;
using StringTools;
using Std;

import stipple.AST;
import haxe.macro.Expr;
import haxe.macro.Context;

#if macro
class ASTUtil {
	public static function toListExpr(list: List<Expr>): Expr {
		return { expr: ECall({ expr: EField({ expr: EConst(CIdent("Lambda")), pos: Context.currentPos() }, "list"), pos: Context.currentPos() }, [ { expr: EArrayDecl(list.array()), pos: Context.currentPos() }]), pos: Context.currentPos() };
	}
	static function reifyPosition(hpos: Position) {
		var pos = Context.getPosInfos(hpos);
		return { expr: EObjectDecl([{ field: "min", expr: { expr: EConst(CInt(Std.string(pos.min))), pos: hpos }}, { field: "max", expr: { expr: EConst(CInt(Std.string(pos.max))), pos: hpos }}, { field: "file", expr: { expr: EConst(CString(pos.file)), pos: hpos }} ]), pos: hpos };
	}
	public static function toExpr(node: Node): Expr {
		return switch (node) {
			case ProgramNode(stmts): 
				{ expr: ECall({ expr: EConst(CIdent("ProgramNode")), pos: Context.currentPos() }, [toListExpr(stmts.map(toExpr))]), pos: Context.currentPos() };
			case ContentNode(str):
				{ expr: ECall({ expr: EConst(CIdent("ContentNode")), pos: Context.currentPos() }, [{ expr: EConst(CString(str)), pos: Context.currentPos() }]), pos: Context.currentPos() };
			case MustacheNode(id, params, hash, escaped):
				{ expr: ECall({ expr: EConst(CIdent("MustacheNode")), pos: Context.currentPos() }, [toExpr(id), toListExpr(params.map(toExpr)), toExpr(hash), { expr: EConst(CIdent(Std.string(escaped))), pos: Context.currentPos() }]), pos: Context.currentPos() };
			case IdNode(parts, depth, isScoped, original, pos):
				{ expr: ECall({ expr: EConst(CIdent("IdNode")), pos: Context.currentPos() }, [toListExpr(parts.map(function (_) return { expr: EConst(CString(_)), pos: Context.currentPos() })), { expr: EConst(CInt(Std.string(depth))), pos: Context.currentPos() }, { expr: EConst(CIdent(Std.string(isScoped))), pos: Context.currentPos() }, { expr: EConst(CString(original)), pos: Context.currentPos() }, reifyPosition(pos)]), pos: Context.currentPos() }
			case HashNode(pairs):
				{ expr: ECall({ expr: EConst(CIdent("HashNode")), pos: Context.currentPos() }, [toListExpr(pairs.map(function (_) return { expr: EObjectDecl([{ expr: { expr: EConst(CString(_.left)), pos: Context.currentPos() }, field: "left" }, { expr: toExpr(_.right), field: "right" }]), pos: Context.currentPos() }))]), pos: Context.currentPos() };
			case NullNode:
				{ expr: EConst(CIdent("NullNode")), pos: Context.currentPos() };
			case CommentNode(str):
				{ expr: ECall({ expr: EConst(CIdent("CommentNode")), pos: Context.currentPos() }, [{ expr: EConst(CString(str)), pos: Context.currentPos() }]), pos: Context.currentPos() };
			case BlockNode(mustache, program, inverse):
				{ expr: ECall({ expr: EConst(CIdent("BlockNode")), pos: Context.currentPos() }, [toExpr(mustache), toExpr(program), toExpr(inverse)]), pos: Context.currentPos() }; 
			case IntegerNode(num):
				{ expr: ECall({ expr: EConst(CIdent("IntegerNode")), pos: Context.currentPos() }, [{ expr: EConst(CInt(Std.string(num))), pos: Context.currentPos() }]), pos: Context.currentPos() };
			case DecimalNode(num):
				{ expr: ECall({ expr: EConst(CIdent("DecimalNode")), pos: Context.currentPos() }, [{ expr: EConst(CFloat(Std.string(num))), pos: Context.currentPos() }]), pos: Context.currentPos() };
			case DataNode(idnode):
				{ expr: ECall({ expr: EConst(CIdent("DataNode")), pos: Context.currentPos() }, [toExpr(idnode)]), pos: Context.currentPos() };
			case BooleanNode(bool):
				{ expr: ECall({ expr: EConst(CIdent("BooleanNode")), pos: Context.currentPos() }, [{ expr: EConst(CIdent(Std.string(bool))), pos: Context.currentPos() }]), pos: Context.currentPos() };
			case StringLiteralNode(str):
				{ expr: ECall({ expr: EConst(CIdent("StringLiteralNode")), pos: Context.currentPos() }, [{ expr: EConst(CString(str)), pos: Context.currentPos() }]), pos: Context.currentPos() };
			case PartialNode(PartialNameNode(str), ctx, hash, pos):
				{ expr: ECall({ expr: EConst(CIdent("PartialNode")), pos: Context.currentPos() }, [{ expr: ECall({ expr: EConst(CIdent("PartialNameNode")), pos: Context.currentPos() }, [{ expr: EConst(CString(str)), pos: Context.currentPos() }]), pos: Context.currentPos() }, toExpr(ctx), toExpr(hash), reifyPosition(pos)]), pos: Context.currentPos() };
			case v: Context.error('AST node "${v}" cannot be converted to a haxe compile-time expression', Context.currentPos());
		}
	}
	static function fromMacroPosition (pos: Expr) {
		trace(pos);
		return switch (pos) {
			case { expr: EObjectDecl([{ field: "min", expr: { expr: EConst(CInt(min)), pos: _ }}, { field: "max", expr: { expr: EConst(CInt(max)), pos: _ }}, { field: "file", expr: { expr: EConst(CString(file)), pos: _ }}]), pos: _ }:
				Context.makePosition({ min: Std.parseInt(min), max: Std.parseInt(max), file: file });
			case _:
				Context.warning("Failed to determine macro position from reified object declaration: " + pos, Context.currentPos());
				Context.currentPos();
		}
	}
	public static function toNode(expr: Expr): Node {
		return switch (expr) {
			case { expr: ECall({ expr: EConst(CIdent("ProgramNode")), pos: _ }, [{ expr: ECall({ expr: EField({ expr: EConst(CIdent("Lambda")), pos: _ }, "list"), pos: _ }, [{ expr: EArrayDecl(stmts), pos: _ }]), pos: _ }]), pos: _ }:
				ProgramNode(stmts.map(toNode).list());
			case { expr: ECall({ expr: EConst(CIdent("ContentNode")), pos: _ }, [{ expr: EConst(CString(str)), pos: _ }]), pos: _ }:
				ContentNode(str);
			case { expr: ECall({ expr: EConst(CIdent("MustacheNode")), pos: _ }, [id, { expr: ECall({ expr: EField({ expr: EConst(CIdent("Lambda")), pos: _ }, "list"), pos: _ }, [ { expr: EArrayDecl(params), pos: _ }]), pos: _ }, hash, { expr: EConst(CIdent(bool)), pos: _ }]), pos: _ }:
				MustacheNode(toNode(id), params.map(toNode).list(), toNode(hash), bool == "true" ? true : false);
			case { expr: ECall({ expr: EConst(CIdent("IdNode")), pos: _ }, [{ expr: ECall({ expr: EField({ expr: EConst(CIdent("Lambda")), pos: _ }, "list"), pos: _ }, [ { expr: EArrayDecl(parts), pos: _ }]), pos: _ }, { expr: EConst(CInt(depth)), pos: _ }, { expr: EConst(CIdent(bool)), pos: _ }, { expr: EConst(CString(original)), pos: _ }, mpos]), pos: _ }:
				var pos = fromMacroPosition(mpos);
				IdNode(parts.map(function (_) return switch (_) {
					case { expr: EConst(CString(s)), pos: _ }: s;
					case v: Context.error('Expression ${v} expected to be string constant while converting to AST node', Context.currentPos());
				}).list(), Std.parseInt(depth), bool == "true" ? true : false, original, pos);
			case { expr: ECall({ expr: EConst(CIdent("HashNode")), pos: _ }, [{ expr: ECall({ expr: EField({ expr: EConst(CIdent("Lambda")), pos: _ }, "list"), pos: _ }, [ { expr: EArrayDecl(pairs), pos: _ }]), pos: _ }]), pos: _ }:
				HashNode(pairs.map(function (_) return switch (_) {
					case { expr: EObjectDecl([{ expr: { expr: EConst(CString(left)), pos: _ }, field: "left" }, { expr: right, field: "right" }]), pos: _ }: { left: left, right: toNode(right) };
					case v: Context.error('Expression ${v} expected to be object declaration while converting to AST node', Context.currentPos());
				}).list());
			case { expr: ECall({ expr: EConst(CIdent("NullNode")), pos: _ }, []), pos: _ }:
				NullNode;
			case { expr: ECall({ expr: EConst(CIdent("BlockNode")), pos: _ }, [mustache, program, inverse]), pos: _ }:
				BlockNode(toNode(mustache), toNode(program), toNode(inverse));
			case { expr: ECall({ expr: EConst(CIdent("IntegerNode")), pos: _ }, [{ expr: EConst(CInt(num)), pos: _ }]), pos: _ }:
				IntegerNode(Std.parseInt(num));
			case { expr: ECall({ expr: EConst(CIdent("DecimalNode")), pos: _ }, [{ expr: EConst(CFloat(num)), pos: _ }]), pos: _ }:
				DecimalNode(Std.parseFloat(num));
			case { expr: ECall({ expr: EConst(CIdent("DataNode")), pos: _ }, [idnode]), pos: _ }:
				DataNode(toNode(idnode));
			case { expr: ECall({ expr: EConst(CIdent("BooleanNode")), pos: _ }, [{ expr: EConst(CIdent(bool)), pos: _}]), pos: _}:
				BooleanNode(bool == "true" ? true : false);
			case { expr: ECall({ expr: EConst(CIdent("StringLiteralNode")), pos: _ }, [{ expr: EConst(CString(str)), pos: _}]), pos: _ }:
				StringLiteralNode(str);
			case { expr: ECall({ expr: EConst(CIdent("PartialNode")), pos: _ }, [{ expr: ECall({ expr: EConst(CIdent("PartialNameNode")), pos: _ }, [{ expr: EConst(CString(str)), pos: _ }]), pos: _ }, ctxnode, hashnode, mpos]), pos: _ }:
				PartialNode(PartialNameNode(str), toNode(ctxnode), toNode(hashnode), fromMacroPosition(mpos));
			case v: Context.error('Expression "${v}" cannot be converted to a template AST node', Context.currentPos());
		}
	}
}
#end
