package stipple;

import hxparse.Lexer;
import hxparse.RuleBuilder;
import hxparse.Parser;
import hxparse.ParserBuilder;
import hxparse.TokenSource;

import stipple.AST;

private enum Token {
	TOpen(str: String, pos: Int);
	TId(str: String, pos: Int);
	TClose(pos: Int);
	TContent(str: String, pos: Int);
	TOpenUnescaped(pos: Int);
	TCloseUnescaped(pos: Int);
	TOpenBlock(pos: Int);
	TOpenInverse(str: String, pos: Int);
	TOpenEndBlock(pos: Int);
	TSimpleInverse(pos: Int);
	TSep(str: String, pos: Int);
	TOpenPartial(pos: Int);
	TStringLiteral(str: String, pos: Int);
	TInteger(num: String, pos: Int);
	TDecimal(dec: String, pos: Int);
	TBoolean(bool: String, pos: Int);
	TEquals(pos: Int);
	TData(pos: Int);
	TComment(comment: String, pos: Int);
	TWhiteSpace;
	TEof;
}

class Grammar extends Lexer {
	static public var buf: StringBuf;

	static public var tok = Lexer.buildRuleset([
		{
			rule: "",
			func: function(lexer) return TOpen(lexer.current, lexer.curPos().pmin)
		},
		{
			rule: "{{",
			func: function(lexer) return TOpen(lexer.current, lexer.curPos().pmin)
		},
		{
			rule: "{{&",
			func: function(lexer) return TOpen(lexer.current, lexer.curPos().pmin)
		},
		{
			rule: "}}",
			func: function(lexer) return TClose(lexer.curPos().pmin)
		},
		{
			rule: "{{{",
			func: function(lexer) return TOpenUnescaped(lexer.curPos().pmin)
		},
		{
			rule: "}}}",
			func: function(lexer) return TCloseUnescaped(lexer.curPos().pmin)
		},
		{
			rule: "{{>",
			func: function(lexer) return TOpenPartial(lexer.curPos().pmin)
		},
		{
			rule: "{{#",
			func: function(lexer) return TOpenBlock(lexer.curPos().pmin)
		},
		{
			rule: "{{^",
			func: function(lexer) return TOpenInverse(lexer.current, lexer.curPos().pmin)
		},
		{
			rule: "{{/",
			func: function(lexer) return TOpenEndBlock(lexer.curPos().pmin)
		},
		{
			rule: "{{[ \t\r\n]*else",
			func: function(lexer) return TOpenInverse(lexer.current, lexer.curPos().pmin)
		},
		{
			rule: "\\/",
			func: function(lexer) return TSep(lexer.current, lexer.curPos().pmin)
		},
		{
			rule: "\\.",
			func: function(lexer) return TSep(lexer.current, lexer.curPos().pmin)
		},
		{
			rule: '"',
			func: function(lexer) {
				var pos = lexer.curPos().pmin;
				buf = new StringBuf();
				buf.add('"');
				lexer.token(stringLiteralDouble);
				buf.add('"');
				return TStringLiteral(buf.toString(), pos);
			}
		},
		{
			rule: "'",
			func: function(lexer) {
				var pos = lexer.curPos().pmin;
				buf = new StringBuf();
				buf.add("'");
				lexer.token(stringLiteralSingle);
				buf.add("'");
				return TStringLiteral(buf.toString(), pos);
			}
		},
		{
			rule: "\\-?[0-9]+[^0-9/.]",
			func: function(lexer) {
				var minPos = lexer.curPos().pmin;
				lexer.pos--;
				return TInteger(Std.string(Std.parseInt(lexer.current)), minPos);
			}
		},
		{
			rule: "\\-?[0-9]+\\.([0-9]+)?",
			func: function (lexer) {
				return TDecimal(lexer.current, lexer.curPos().pmin);
			}
		},
		{
			rule: "true",
			func: function(lexer) return TBoolean(lexer.current, lexer.curPos().pmin)
		},
		{
			rule: "false",
			func: function(lexer) return TBoolean(lexer.current, lexer.curPos().pmin)
		},
		{
			rule: "=",
			func: function(lexer) return TEquals(lexer.curPos().pmin)
		},
		{
			rule: "@",
			func: function(lexer) return TData(lexer.curPos().pmin)
		},
		{
			rule: "\\.\\.",
			func: function(lexer) return TId(lexer.current, lexer.curPos().pmin)
		},
		{
			rule: "\\.([=}\\/. ])",
			func: function(lexer) {
				var minPos = lexer.curPos().pmin;
				lexer.pos--;
				return TId(".", minPos);
			}
		},
		{
			rule: "\\[[^\\]]*\\]",
			func: function(lexer) return TId(lexer.current, lexer.curPos().pmin)
		},
		{
			rule: "[^ !\"#%-,\n\\.\\/;->@\\[-\\^`\\{-~]+",
			func: function(lexer) return TId(lexer.current, lexer.curPos().pmin)
		},
		{
			rule: "[ \t\r\n]+",
			func: function(_) return TWhiteSpace
		}

	]);

	static public var content = Lexer.buildRuleset([
		{
			rule: "",
			func: function(_) return TEof
		},
		{
			rule: "{{",
			func: function(lexer) return TOpen(lexer.current, lexer.curPos().pmin)
		},
		{
			rule: "\\\\{{{?[^\x00\\\\{]+",
			func: function(lexer) return TContent(lexer.current, lexer.curPos().pmin)
		},
		{
			rule: "\\\\\\\\",
			func: function(lexer) return TContent(lexer.current, lexer.curPos().pmin)
		},
		{
			rule: "{{{",
			func: function(lexer) return TOpenUnescaped(lexer.curPos().pmin)
		},
		{
			rule: "{{&",
			func: function(lexer) return TOpen(lexer.current, lexer.curPos().pmin)
		},
		{
			rule: "{{>",
			func: function(lexer) return TOpenPartial(lexer.curPos().pmin)
		},
		{
			rule: "{{#",
			func: function(lexer) return TOpenBlock(lexer.curPos().pmin)
		},
		{
			rule: "{{^",
			func: function(lexer) return TOpenInverse(lexer.current, lexer.curPos().pmin)
		},
		{
			rule: "{{[ \t\r\n]*else",
			func: function(lexer) return TOpenInverse(lexer.current, lexer.curPos().pmin)
		},
		{
			rule: "{{/",
			func: function(lexer) return TOpenEndBlock(lexer.curPos().pmin)
		},
		{
			rule: "{{!--",
			func: function(lexer) {
				var pos = lexer.curPos().pmax;
				buf = new StringBuf();
				lexer.token(commentLong);
				return TComment(buf.toString(), pos);
			}
		}, 
		{
			rule: "{{!",
			func: function(lexer) {
				var pos = lexer.curPos().pmax;
				buf = new StringBuf();
				lexer.token(comment);
				return TComment(buf.toString(), pos);
			}
		},
		{
			rule: "[^\x00{]+{[^{]",
			func: function(lexer) {
				return TContent(lexer.current, lexer.curPos().pmin);
			}
		},
		{
			rule: "[^\x00\\\\{]+",
			func: function(lexer) return TContent(lexer.current, lexer.curPos().pmin)
		},
		{
			rule: "\\\\[^\x00{]*",
		 	func: function(lexer) return TContent(lexer.current, lexer.curPos().pmin)
		},
	]);

	static public var commentLong = Lexer.buildRuleset([
		{
			rule: "--}}",
			func: function(lexer) return lexer.curPos().pmax
		},
		{
			rule: "[^\x00\\-]+",
			func: function(lexer) {
				buf.add(lexer.current);
				return lexer.token(commentLong);
			}
		}
	]);

	static public var comment = Lexer.buildRuleset([
		{
			rule: "}}",
			func: function(lexer) return lexer.curPos().pmax
		},
		{
			rule: "[^\x00}]+",
			func: function(lexer) {
				buf.add(lexer.current);
				return lexer.token(comment);
			}
		}
	]);

	static public var stringLiteralDouble = Lexer.buildRuleset([
		{
			rule: "\\\\t",
			func: function(lexer) {
				buf.addChar("\t".code);
				return lexer.token(stringLiteralDouble);
			}
		},
		{
			rule: "\\\\n",
			func: function(lexer) {
				buf.addChar("\n".code);
				return lexer.token(stringLiteralDouble);
			}
		},
		{
			rule: "\\\\r",
			func: function(lexer) {
				buf.addChar("\r".code);
				return lexer.token(stringLiteralDouble);
			}
		},
		{
			rule: '\\\\"',
			func: function(lexer) {
				buf.addChar('\\'.code);
				buf.addChar('"'.code);
				return lexer.token(stringLiteralDouble);
			}
		},
		{
			rule: "\\\\u[0-9A-Fa-f][0-9A-Fa-f][0-9A-Fa-f][0-9A-Fa-f]",
			func: function(lexer) {
				buf.add(String.fromCharCode(Std.parseInt("0x" +lexer.current.substr(2))));
				return lexer.token(stringLiteralDouble);
			}
		},
		{
			rule: '"',
			func: function(lexer) return lexer.curPos().pmax
		},
		{
			rule: '[^"]',
			func: function(lexer) {
				buf.add(lexer.current);
				return lexer.token(stringLiteralDouble);
			}
		},
	]);

	static public var stringLiteralSingle = Lexer.buildRuleset([
		{
			rule: "\\\\t",
			func: function(lexer) {
				buf.addChar("\t".code);
				return lexer.token(stringLiteralSingle);
			}
		},
		{
			rule: "\\\\n",
			func: function(lexer) {
				buf.addChar("\n".code);
				return lexer.token(stringLiteralSingle);
			}
		},
		{
			rule: "\\\\r",
			func: function(lexer) {
				buf.addChar("\r".code);
				return lexer.token(stringLiteralSingle);
			}
		},
		{
			rule: "\\\\'",
			func: function(lexer) {
				buf.addChar('\\'.code);
				buf.addChar("'".code);
				return lexer.token(stringLiteralSingle);
			}
		},
		{
			rule: "\\\\u[0-9A-Fa-f][0-9A-Fa-f][0-9A-Fa-f][0-9A-Fa-f]",
			func: function(lexer) {
				buf.add(String.fromCharCode(Std.parseInt("0x" +lexer.current.substr(2))));
				return lexer.token(stringLiteralSingle);
			}
		},
		{
			rule: "'",
			func: function(lexer) return lexer.curPos().pmax
		},
		{
			rule: "[^']",
			func: function(lexer) {
				buf.add(lexer.current);
				return lexer.token(stringLiteralSingle);
			}
		},
	]);

}

class HpsTokenSource {
	var lexer: hxparse.Lexer;
	var insideMustache: Bool;
	public function new (lexer) {
		this.lexer = lexer;
		this.insideMustache = false;
	}

	public function token(): Token {
		if (insideMustache) {
			var tok = lexer.token(Grammar.tok);
			switch tok {
				case TClose(_) 
					 | TCloseUnescaped(_): 
						 insideMustache = false;
				case TWhiteSpace: 
					// skip whitespace
					return token();
				case _:
			}
			return tok;
		} else {
			var tok = lexer.token(Grammar.content);
			switch tok {
				case TOpen(_) 
					 | TOpenUnescaped(_) 
					 | TOpenPartial(_) 
					 | TOpenBlock(_) 
					 | TOpenInverse(_) 
					 | TOpenEndBlock(_): 
						 insideMustache = true;
				case _:
			}
			return tok;
		}
	}

	public function curPos(): hxparse.Position {
		return lexer.curPos();
	}
}

class HpsParser extends Parser<HpsTokenSource, Token> {

	public function new (input: byte.ByteData) {
		var lexer = new Grammar(input);
		var ts = new HpsTokenSource(lexer);
		super(ts);
	}

	public function parse (): ASTExprDef {
		return parseStatements();
	}

 function parseStatements() {
		var a: Array<ASTExprDef> = [];
		while (true) {
			Parser.parse(
				switch stream {
					case [TEof]: return List(a);
					case [st = parseStatement()]: 
						a.push(st);
					case _: 
						unexpected();
				}
			);
		}
		return List(a);
 }

	function parseStatement() {
		return Parser.parse(
			switch stream {
				case [c = parseContent()]: c;
				case [mu = parseMustache()]: mu;
				case [TOpenBlock(pOpen), inMustache = parseInMustache(), TClose(pClose)]:
					var a = [Complex({name: "OPEN_BLOCK", value: Str("{{#")}, pOpen)].concat(inMustache);
					a.push(Complex({name: "CLOSE", value: Str("}}")}, pClose));
					List(a.concat(parseBlock([])));
				
				case [TOpenInverse(s, pOpen), inMustache = parseInMustache(), TClose(pClose)]:
					var a = [Complex({name: "OPEN_INVERSE", value: Str("{{^")}, pOpen)].concat(inMustache);
					a.push(Complex({name: "CLOSE", value: Str("}}")}, pClose));
					if (inMustache.length > 0) {
						List(a.concat(parseBlock([])));
					} else {
						// this handles simpleInverse
						List(a);
					} 
			}
		);
	}

	function parseBlock(stmts: Array<ASTExprDef>) {
		var a = [];
		while (true) {
			Parser.parse(
				switch stream {
					case [st = parseStatement()]:
						stmts.push(st);
					case [TOpenEndBlock(pOpen), inMustache = parseInMustache(), TClose(pClose)]: 
						if (stmts.length > 0) {
							a.push(Complex({name: "STATEMENTS", value: List(stmts)}, pOpen));
						}
						a.push(Complex({name: "OPEN_ENDBLOCK", value: Str("{{/")}, pOpen));
						inMustache.push(Complex({name: "CLOSE", value: Str("}}")}, pClose));
						return a.concat(inMustache);
					case _: 
						return a;
				}
			);
		}

		return a;
	}

	function parseContent() {
		return Parser.parse(
			switch stream {
				case [TContent(s, pContent)]: 
					Complex({name: "INNERCONTENT", value: Str(s)}, pContent);
				case [TComment(comment, pos)]:
					Complex({name: "COMMENT", value: Str(comment)}, pos);
			}
		);
	}

	function parseMustache() {
		return Parser.parse(
			switch stream {
				case [TOpenUnescaped(pOpen), inMustache = parseInMustache(), TCloseUnescaped(pClose)]:
					var a = [Complex({name: "OPEN_UNESCAPED", value: Str("{{{")}, pOpen)].concat(inMustache);
					a.push(Complex({name: "CLOSE_UNESCAPED", value: Str("}}}")}, pClose));
					List(a);
				case [TOpen(s, pOpen), inMustache = parseInMustache(), TClose(pClose)]:
					var a = [Complex({name: "OPEN", value: Str(s)}, pOpen)].concat(inMustache);
					a.push(Complex({name: "CLOSE", value: Str("}}")}, pClose));
					List(a);
				case [TOpenPartial(pOpen), partial = parsePartial(), TClose(pClose)]:
					var a = [Complex({name: "OPEN_PARTIAL", value: Str("{{>")}, pOpen)].concat(partial);
					a.push(Complex({name: "CLOSE", value: Str("}}")}, pClose));
					List(a);
			}
		);
	}

	function parseInMustache() {
		return Parser.parse(
			switch stream {
				case [a = parsePathSegments()]:
					while (true) {
						switch stream {
							case [TDecimal(dec, pos)]: 
								a.push(Complex({name: "DECIMAL", value: Str(dec)}, pos));
							case [TInteger(num, pos)]: 
								a.push(Complex({name: "INTEGER", value: Str(num)}, pos));
							case [TBoolean(bool, pos)]: 
								a.push(Complex({name: "BOOLEAN", value: Str(bool)}, pos));
							case [TStringLiteral(str, pos)]: 
								a.push(Complex({name: "STRINGLITERAL", value: Str(str)}, pos));
							case [hash = parseParamOrHash()]:
								a.push(List(hash));
							case _: 
								return a;
						}
					}
					a;
				case [TData(pos)]:
					switch stream {
						case [pathSegments = parsePathSegments()]:
								[Complex({name: "DATA", value: Str("@")}, pos)].concat(pathSegments);
					}
				case _: return [];
			}
		);
	}

	function parsePathSegments() {
		var a: Array<ASTExprDef> = [];
		Parser.parse(
			switch stream {
				case [TId(ident, pos)]: 
					a.push(Complex({name: "ID", value: Str(ident)}, pos));
					while (true) {
						switch stream {
							case [TSep(sep, pos1), TId(ident, pos2)]: 
								a.push(List([
									Complex({name: "SEP", value: Str(sep)}, pos1), 
									Complex({name: "ID", value: Str(ident)}, pos2)
								]));
							case _: 
								return a;
						}
					}
			}
		);
		return a;
	}

	function parsePartial() {
		return Parser.parse(
			switch stream {
				case [partialName = parsePartialName()]:
					switch stream {
						case [TDecimal(dec, pos)]: 
							partialName.push(Complex({name: "DECIMAL", value: Str(dec)}, pos));
						case [TInteger(num, pos)]: 
							partialName.push(Complex({name: "INTEGER", value: Str(num)}, pos));
						case [TBoolean(bool, pos)]: 
							partialName.push(Complex({name: "BOOLEAN", value: Str(bool)}, pos));
						case [TStringLiteral(str, pos)]: 
							partialName.push(Complex({name: "STRINGLITERAL", value: Str(str)}, pos));
						case [hash = parseParamOrHash()]:
							partialName = partialName.concat(hash);
						case _: 
					}
					partialName;
			}
		);
	}

	function parsePartialName(): Array<ASTExprDef> {
		return Parser.parse(
			switch stream {
				case [TStringLiteral(str, pos)]: [Complex({name: "STRINGLITERAL", value: Str(str)}, pos)];
				case [TInteger(num, pos)]: [Complex({name: "INTEGER", value: Str(num)}, pos)];
				case [pathSegments = parsePathSegments()]: pathSegments;
			}
		);
	}

	function parseParam() {
		return Parser.parse(
			switch stream {
				case [TDecimal(dec, pos)]: Complex({name: "DECIMAL", value: Str(dec)}, pos);
				case [TInteger(num, pos)]: Complex({name: "INTEGER", value: Str(num)}, pos);
				case [TBoolean(bool, pos)]: Complex({name: "BOOLEAN", value: Str(bool)}, pos);
				case [TStringLiteral(str, pos)]: Complex({name: "STRINGLITERAL", value: Str(str)}, pos);
			}
		);
	}

	function parseParamOrHash() {
		return Parser.parse(
			switch stream {
				case [TData(pos)]:
					switch stream {
						case [param = parseParam()]:
							[Complex({name: "DATA", value: Str("@")}, pos), param];
						case [pathSegments = parsePathSegments()]: 
							[Complex({name: "DATA", value: Str("@")}, pos)].concat(pathSegments);
					}
				case [pathSegments = parsePathSegments()]: 
					switch stream {
						case [TEquals(pos)]:
							pathSegments.push(Complex({name: "EQUALS", value: Str("=")}, pos));
							switch stream {
								case [TData(pData)]:
									pathSegments.push(Complex({name: "DATA", value: Str("@")}, pos));
									switch stream {
										case [param = parseParam()]:
											pathSegments.push(param);
										case [valueSegments = parsePathSegments()]: 
											pathSegments = pathSegments.concat(valueSegments);
									}
								case [param = parseParam()]:
									pathSegments.push(param);
								case [valueSegments = parsePathSegments()]: 
									pathSegments = pathSegments.concat(valueSegments);
							}
							pathSegments;
						case _:
							pathSegments;
					}
			}
		);
	}
}
