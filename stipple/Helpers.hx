package stipple;

import haxe.macro.Expr;
import haxe.macro.Context;
import stipple.Template;

using Lambda;

typedef Core = {
	with: HelperFuncParam,
	each: HelperFuncParam
}


class Helpers {
	static function dynamicToBool(conditional: Dynamic, ctx: Dynamic): Bool {
			return if (Std.is(conditional, Bool)) {
				if (cast(conditional, Bool)) {
					true;
				} else {
					false;
				}
			} else {
				if (Reflect.isFunction(conditional)) {
					dynamicToBool(Reflect.callMethod(null, conditional, [ctx]), ctx);
				} else if (Std.is(conditional, Int)) {
					switch (cast(conditional, Int)) {
						case 0: false;
						case _: true;
					}
				} else {
					switch (Type.getClass(conditional)) {
						case String: true;
						case Array if (Lambda.empty(cast(conditional, Array<Dynamic>))): false;
						case Array: true;
						case _: false;
					}
				}
			}
	}
	static function each (context: Array<RType>, options: HelperOptionsParam): String {
		var data : Data = if (options.data != null) {
			Reflect.copy(options.data);
		} else cast { };
		var cycleOnArray = function (a: Array<Dynamic>) {
			return a.mapi(function (i, _) return {
				data.index = i;
				data.first = (i==0);
				data.last = (i == (a.length - 1));
				options.fn(_, data);
			}).join("");
		}
		var cycleOnObject = function (o: Dynamic) {
			var fields =  Reflect.fields(o);
			fields.sort(function (a, b) return Reflect.compare(a, b));
			return fields.mapi(function (i, _) return {
				var ctx = Reflect.field(o, _);
				data.key = _;
				data.index = i;
				data.first = (i==0);
				options.fn(ctx, data);
			}).join("");
		}
		return switch (context) {
			case [Func(args, cb)]: 
				var a = Reflect.callMethod(null, cb, args);
				if (Std.is(a, Array)) {
					cycleOnArray(cast(a, Array<Dynamic>));
				} else {
					cycleOnObject(a);
				}
			case [Many(a)]: 
				cycleOnArray(a.map(function (_): Dynamic return switch (_) {
					case Boolean(b): b;
					case Func(_, cb): cb;
					case Many(a): a;
					case Object(o): o;
					case Const(s): s;
					case Nothing: null;
				}));
			case [Object(o)]:
				cycleOnObject(o);
			case _: 
				options.inverse(options.ctx, {});
		}
	}
	static function with (context: Array<RType>, options: HelperOptionsParam): String {
		return switch (context) {
			case [Boolean(b)]: options.fn(b, {});
			case [Func(args, cb)]: options.fn(Reflect.callMethod(null, cb, args), {});
			case [Many(a)]: options.fn(a, {});
			case [Const(s)]: options.fn(s, {});
			case [Object(o)]: options.fn(o, {});
			case _: options.inverse(options.ctx, {});
		}
	}
	public static function core<T>(): Core {
		var r: Core = { 
			with: with,
		 	each: each
		}
		// workaround for reserved word "if"
		Reflect.setField(r, "if", function (conditional: Array<RType>, options: HelperOptionsTyped<T, T, T>): String {

			var hash = options.hash; 

			return switch (conditional) {
				case [Boolean(true)]: options.fn(options.ctx, options.data);
				case [Boolean(false)]: options.inverse(options.ctx, options.data);
				case [Func(args, cb)] if (dynamicToBool(Reflect.callMethod(null, cb, args), args[0])): options.fn(options.ctx, options.data);
				case [Object(n)] if (Std.is(n, Int)): 
						switch (cast(n, Int)) {
							case 0: options.inverse(options.ctx, options.data);
							case _: options.fn(options.ctx, options.data);
						}
				case [Many(arr)] if (arr.empty()): options.inverse(options.ctx, options.data);
				case [Many(arr)]: options.fn(options.ctx, options.data);
				case [Const(v)] if (v.length > 0): options.fn(options.ctx, options.data);
				case _: options.inverse(options.ctx, options.data);
			}
		});

		return r;
	}
}
