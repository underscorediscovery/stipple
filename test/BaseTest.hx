package test;

import utest.Assert;

using stipple.Template;
import stipple.AST;
import stipple.Helpers;
import stipple.Lexer;

class BaseTest {

	public function new () { }

	function testMostBasic () {
		Assert.same("foo", new Template ().fromString("{{foo}}").execute({ foo: "foo" }));
	}

	function testEscaping () {
		Assert.same("{{foo}}", new Template ().fromString("\\{{foo}}").execute({ foo: "food" }));
		Assert.same("content {{foo}}", new Template ().fromString("content \\{{foo}}").execute({ foo: "food" }));
		Assert.same("\\\\food", new Template ().fromString("\\\\{{foo}}").execute({ foo: "food" }));
		Assert.same("content \\\\food", new Template ().fromString("content \\\\{{foo}}").execute({ foo: "food" }));
		Assert.same("\\\\ food", new Template ().fromString("\\\\ {{foo}}").execute({ foo: "food" }));
	}

	function testCompilingWithBasicContext () {
		Assert.same("Goodbye\ncruel\nworld!", new Template ().fromString("Goodbye\n{{cruel}}\n{{world}}!").execute({ cruel: "cruel", world: "world" }));
	}

	function testComments () {
		Assert.same("Goodbye\ncruel\nworld!", new Template ().fromString("{{! Goodbye}}Goodbye\n{{cruel}}\n{{world}}!").execute({ cruel: "cruel", world: "world" }));
	}

	function testBoolean () {
		Assert.same("GOODBYE cruel world!", new Template ().fromString("{{#goodbye}}GOODBYE {{/goodbye}}cruel {{world}}!").execute({ goodbye: true, world: "world" }));
		Assert.same("cruel world!", new Template ().fromString("{{#goodbye}}GOODBYE {{/goodbye}}cruel {{world}}!").execute({ goodbye: false, world: "world" }));
	}

	function testZeros () {
		Assert.same("num1: 42, num2: 0", new Template ().fromString("num1: {{num1}}, num2: {{num2}}").execute({ num1: 42, num2: 0 }));
		Assert.same("num: 0", new Template ().fromString("num: {{.}}").execute(0));
		Assert.same("num: 0", new Template ().fromString("num: {{num1/num2}}").execute({num1: {num2: 0}}));
	}

	function testNewlines () {
		Assert.same("Alan's\nTest", new Template ().fromString("Alan's\nTest").execute({}));
		Assert.same("Alan's\rTest", new Template ().fromString("Alan's\rTest").execute({}));
	}

	function testEscapingText () {
		Assert.same("Awesome's", new Template ().fromString("Awesome's").execute({}));
		Assert.same("Awesome\\", new Template ().fromString("Awesome\\").execute({}));
		Assert.same("Awesome\\\\ foo", new Template ().fromString("Awesome\\\\ foo").execute({}));
		Assert.same("Awesome \\", new Template ().fromString("Awesome {{foo}}").execute({ foo: '\\' }));
		Assert.same(' " " ', new Template ().fromString(' " " ').execute({}));
	}

	function testEscapingExpressions () {
		Assert.same('&\"\\<>', new Template ().fromString("{{{awesome}}}").execute({ awesome: "&\"\\<>" }));
		Assert.same('&\"\\<>', new Template ().fromString("{{&awesome}}").execute({ awesome: "&\"\\<>" }));
		Assert.same('&amp;&quot;&#039;\\&lt;&gt;', new Template ().fromString("{{awesome}}").execute({ awesome: "&\"'\\<>" }));
		Assert.same('Escaped, &lt;b&gt; looks like: &amp;lt;b&amp;gt;', new Template ().fromString("{{awesome}}").execute({ awesome: "Escaped, <b> looks like: &lt;b&gt;"}));
	}

	function testNoEscapeSafestrings () {
		Assert.same("&\"\\<>", new Template ().fromString("{{awesome}}").execute({ awesome: function (_, ?opts) { return new SafeString("&\"\\<>"); }}));
	}

	function testFunctions () {
		Assert.same("Awesome", new Template ().fromString("{{awesome}}").execute({ awesome: function (_, ?opts) { return "Awesome"; }}));
		var more = "More Awesome";
		Assert.same("More Awesome", new Template ().fromString("{{awesome}}").execute({ awesome: function (_, ?opts) { return more; }}));
	}

	function testFunctionsWithContextArgument () {
		Assert.same("Frank", new Template ().fromString("{{awesome frank}}").execute({ awesome: function (context: Array<RType>, options: HelperOptions) { return context.extract(); }, frank: "Frank"}));
	}

	function testBlockFunctionsWithContextArgument () {
		Assert.same("inner 1", new Template ().fromString("{{#awesome 1}}inner {{.}}{{/awesome}}").execute({ awesome: function (context: Array<RType>, options: HelperOptionsTyped<String, String, String>) { return options.fn(context.extract(), {}); }}));
	}

	// UNSUPPORTED DUE TO X-PLATFORM
	// -> all helpers must be typed with two arguments
	//function testBlockFunctionsWithoutContextArgument () {
	//	Assert.same("inner", Template.compile("{{#awesome}}inner{{/awesome}}", { awesome: function (options) { return options.fn(this); }}));
	//} 

	function testPathsWithHyphens () {
		 Assert.same("baz", new Template ().fromString("{{foo-bar}}").execute({ "foo-bar": "baz" }));
		 Assert.same("baz", new Template ().fromString("{{foo.foo-bar}}").execute({ foo: { "foo-bar": "baz" } }));
		 Assert.same("baz", new Template ().fromString("{{foo/foo-bar}}").execute({ foo: { "foo-bar": "baz" } }));
	}

	function testNestedPaths () {
		Assert.same("Goodbye beautiful world!", new Template ().fromString("Goodbye {{alan/expression}} world!").execute({ alan: { expression: "beautiful" }}));
	}

	function testNestedPathsWithEmptyStringValue () {
		Assert.same("Goodbye  world!", new Template ().fromString("Goodbye {{alan/expression}} world!").execute({ alan: { expression: "" }}));
	}

	function testLiteralPaths () {
		Assert.same("Goodbye beautiful world!", new Template ().fromString("Goodbye {{[@alan]/expression}} world!").execute({ "@alan": { expression: "beautiful" }}));
		Assert.same("Goodbye beautiful world!", new Template ().fromString("Goodbye {{[foo bar]/expression}} world!").execute({ "foo bar": { expression: "beautiful" }}));
	}
	
	function testLiteralReferences () {
		Assert.same("Goodbye beautiful world!", new Template ().fromString("Goodbye {{[foo bar]}} world!").execute({"foo bar": "beautiful"}));
	}

	function testCurrentContextPathNoHelpers () {
		Assert.same("test: ", new Template ().fromString("test: {{.}}").execute({ helper: "awesome" }));
	}

	function testComplexButEmptyPaths () {
		Assert.same("", new Template ().fromString("{{person/name}}").execute({ person: { name: null }}));
		Assert.same("", new Template ().fromString("{{person/name}}").execute({ person: {}}));
	}

	function testThisKeywordInPaths () {
		Assert.same("goodbyeGoodbyeGOODBYE", new Template ().fromString("{{#goodbyes}}{{this}}{{/goodbyes}}").execute({goodbyes:["goodbye", "Goodbye", "GOODBYE"]}));
		Assert.same("helloHelloHELLO", new Template ().fromString("{{#hellos}}{{this/text}}{{/hellos}}").execute({hellos:[{text: "hello"}, {text: "Hello"}, {text: "HELLO"}]}));
	}

	function testThisKeywordNestedInsidePath () {
		Assert.raises(function () AST.parse(new HpsParser(byte.ByteData.ofString("{{#hellos}}{{text/this/foo}}{{/hellos}}")).parse()), String);
	}

	function testThisKeywordInHelpers () {
		Assert.same("bar goodbyebar Goodbyebar GOODBYE", new ThisKeywordInHelpers ().fromString("{{#goodbyes}}{{foo this}}{{/goodbyes}}").execute({ goodbyes: ["goodbye", "Goodbye", "GOODBYE"] }));

		Assert.same("bar hellobar Hellobar HELLO", new ThisKeywordInHelpers ().fromString("{{#hellos}}{{foo this/text}}{{/hellos}}").execute({ hellos: [{text: "hello"}, {text: "Hello"}, {text: "HELLO"}] }));
	}
	
	function testNonmatchingCloseBlock () {
		Assert.raises(function () AST.parse(new HpsParser(byte.ByteData.ofString("{{dice}}{{hello}}{{/dice}}")).parse()), hxparse.Unexpected);
	}

}

class ThisKeywordInHelpers<T> extends Template<T> {
	override function localHelpers() return mergeHelpers({
		foo: function (value: Array<RType>, options: HelperOptions) { return "bar " + value.extract(); }
	}, Helpers.core());
}
