package test;


import haxe.macro.Expr;
import haxe.macro.Context;

import utest.Runner;
import utest.TestResult;
import utest.ui.text.PrintReport;

import stipple.AST;
import stipple.Lexer;

class TestSuite {
	#if !macro

	static function main () {
		var runner = new Runner ();
		runner.addCase(new TokenizerTest());
		runner.addCase(new ParserTest());
		runner.addCase(new BaseTest());
		runner.addCase(new BlocksTest());
		runner.addCase(new BuiltinTest());
		runner.addCase(new DataTest());
		runner.addCase(new HelpersTest());
		runner.addCase(new PartialsTest());
		runner.addCase(new StringParamsTest());
		runner.addCase(new UtilsTest());
		new PrintReport(runner);
		runner.run();
	}

	#end

}
