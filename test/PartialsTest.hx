package test;

import utest.Assert; 

import stipple.AST; 
import stipple.Helpers;
using stipple.Template;

using Lambda;

class PartialsTest {
	public function new () {}

	function testBasicPartials () {
		var hash = {dudes: [{name: "Yehuda", url: "http://yehuda"}, {name: "Alan", url: "http://alan"}] };
		Assert.same("Dudes: Yehuda (http://yehuda) Alan (http://alan) ", new BasicPartials ().fromString("Dudes: {{#dudes}}{{> dude}}{{/dudes}}").execute(hash));
	}

	function testPartialsWithContext () {
		var hash = {dudes: [{name: "Yehuda", url: "http://yehuda"}, {name: "Alan", url: "http://alan"}], dude: new Template ().fromString("{{#this}}{{name}} ({{url}}) {{/this}}")};
		Assert.same("Dudes: Yehuda (http://yehuda) Alan (http://alan) ", new Template ().fromString("Dudes: {{>dude dudes}}").execute(hash));
	}

	function testPartialsWithUndefinedContext () {
		var hash = { dude: new Template ().fromString("{{foo}} Empty")};
		Assert.same("Dudes:  Empty", new Template ().fromString("Dudes: {{>dude dudes}}").execute(hash));
	}

	function testPartialsWithParameters () {
		var hash = {foo: 'bar', dudes: [{name: "Yehuda", url: "http://yehuda"}, {name: "Alan", url: "http://alan"}]};
		Assert.same("Dudes: barYehuda (http://yehuda) barAlan (http://alan) ", new PartialsWithParameters ().fromString("Dudes: {{#dudes}}{{> dude others=..}}{{/dudes}}").execute(hash));
	}

	function testPartialInAPartial () {
		var hash = {dudes: [{name: "Yehuda", url: "http://yehuda"}, {name: "Alan", url: "http://alan"}]};
		Assert.same("Dudes: Yehuda <a href='http://yehuda'>http://yehuda</a> Alan <a href='http://alan'>http://alan</a> ", new PartialInAPartial ().fromString("Dudes: {{#dudes}}{{>dude}}{{/dudes}}").execute(hash));
	}

	function testRenderingUndefinedPartialThrowsAnException () {
		Assert.raises(function () return new Template ().fromString("{{> whatever}}").execute({}), String);
	}

	function testAPartialPrecedingASelector () {
		var hash = {name: "Jeepers", another_dude: "Creepers", dude: new Template ().fromString("{{name}}")};
		Assert.same("Dudes: Jeepers Creepers", new Template ().fromString("Dudes: {{>dude}} {{another_dude}}").execute(hash));
	}

	function testPartialsWithSlashPaths () {
		var hash = {name:"Jeepers", another_dude:"Creepers", "shared/dude": new Template ().fromString("{{name}}")};
		Assert.same("Dudes: Jeepers", new Template ().fromString("Dudes: {{> shared/dude}}").execute(hash));
	}

	function testPartialsWithSlashAndPointPaths () {
		var hash = {name:"Jeepers", another_dude:"Creepers", "shared/dude.thing": new Template ().fromString("{{name}}")};

		Assert.same("Dudes: Jeepers", new Template ().fromString("Dudes: {{> shared/dude.thing}}").execute(hash));
	}

	//function testGlobalPartials () {
	//	var partials = new Map();
	//	Template.registerPartial("global_test", new Template().fromString("{{another_dude}}"));

	//	var dude = new Template ().fromString("{{name}}");
	//	var hash = {name:"Jeepers", another_dude:"Creepers"};

	//	Assert.same("Dudes: Jeepers Creepers", new Template ().fromString("Dudes: {{> shared/dude}} {{> global_test}}").execute(hash, {}, {}, { 'shared/dude': dude }));

	//	Template.resetPartials();
	//}

	function testMultiplePartialRegistration () {
		var hash = {name:"Jeepers", another_dude:"Creepers", "shared/dude": new Template ().fromString('{{name}}'), "global_test": new Template ().fromString('{{another_dude}}')};
		Assert.same("Dudes: Jeepers Creepers", new Template ().fromString("Dudes: {{> shared/dude}} {{> global_test}}").execute(hash));
	}

	// not supported in haxe ?
	//function testPartialsWithIntegerPath () {
	//	var dude = new Template ().fromString("{{name}}");
	//	var hash = {name:"Jeepers", another_dude:"Creepers"};
	//	
	//	Assert.same("Dudes: Jeepers", new Template ().fromString("Dudes: {{> 404}}").execute(hash, {}, {}, { 404: dude }));
	//}

 	function testPartialsWithComplexPath () {
		var hash = {name:"Jeepers", another_dude:"Creepers", '404/asdf?.bar': new Template ().fromString("{{name}}")};
		Assert.same("Dudes: Jeepers", new Template ().fromString("Dudes: {{> 404/asdf?.bar}}").execute(hash));
	}

	function testPartialsWithEscaped () {
		var hash = {name:"Jeepers", another_dude:"Creepers", '+404/asdf?.bar': new Template ().fromString("{{name}}")};
		Assert.same("Dudes: Jeepers", new Template ().fromString("Dudes: {{> [+404/asdf?.bar]}}").execute(hash));
	}

	function testPartialsWithString () {
		var hash = {name:"Jeepers", another_dude:"Creepers", '+404/asdf?.bar': new Template ().fromString("{{name}}")};
		Assert.same("Dudes: Jeepers", new Template ().fromString("Dudes: {{> \"+404/asdf?.bar\"}}").execute(hash));
	}

	function testShouldHandleEmptyPartial () {
		var hash = {dudes: [{name: "Yehuda", url: "http://yehuda"}, {name: "Alan", url: "http://alan"}]};

		Assert.same("Dudes: ", new ShouldHandleEmptyPartial ().fromString("Dudes: {{#dudes}}{{> dude}}{{/dudes}}").execute(hash));
	}

	function testPartialWithNestedHelpers () {
		Assert.same("creepy creepy world", new Template().fromString("{{baz}} {{>dude}}").execute({
			baz: function (_, o) return "creepy",
			zap: function (_, o) return "foo",
			dude: new PartialWithNestedHelpers ().fromString("{{baz}} {{zap}}")
		}));
	}
}

class BasicPartials<T> extends Template<T> {
	override function localPartials(): Dynamic<Template<T>> return { dude: new Template ().fromString("{{name}} ({{url}}) ") };
}

class PartialsWithParameters<T> extends Template<T> {
	override function localPartials(): Dynamic<Template<T>> return {
		dude: new Template ().fromString("{{others.foo}}{{name}} ({{url}}) ")
	}
}

class PartialInAPartial<T> extends Template<T> {
	override function localPartials(): Dynamic<Template<T>> return {
		dude: new Template ().fromString("{{name}} {{> url}} "), 
		url:  new Template ().fromString("<a href='{{url}}'>{{url}}</a>")
	}
}

class ShouldHandleEmptyPartial<T> extends Template<T> {
	override function localPartials(): Dynamic<Template<T>> return {
		dude: new Template ().fromString("")
	}
}

class PartialWithNestedHelpers<T> extends Template<T> {
	override function localHelpers() return mergeHelpers({ zap: function (_, o) return "world" }, Helpers.core());
}
