package test;

import utest.Assert; 

import stipple.AST; 
import stipple.Helpers;
using stipple.Template;

using Lambda;

class StringParamsTest {
	public function new () {}

  function testArgumentsToHelpers () {
    var wycats: HelperFunc = function (params, options) return switch (options.params) {
      case [Object(passiveVoice), Object(noun)]:
				'HELP ME MY BOSS ${passiveVoice} ${noun}';
      case _: throw "Invalid parameters: " + params;
    }

    Assert.same("HELP ME MY BOSS is.a slave.driver", new Template ().fromString("{{wycats is.a slave.driver}}").execute({ wycats: wycats }));
  }

	function testBlockForm () {
		var wycats: HelperFunc = function (params, options) return switch (options.params) {
      case [Object(passiveVoice), Object(noun)]:
				'HELP ME MY BOSS ${passiveVoice} ${noun}: ${options.fn(options.ctx, {})}';
      case _: throw "Invalid parameters: " + params;
		}

		Assert.same("HELP ME MY BOSS is.a slave.driver: help :(", new Template ().fromString("{{#wycats is.a slave.driver}}help :({{/wycats}}").execute({ wycats: wycats }));

	}

	function testInsideABlockWithScoped () {
		Assert.same("STOP ME FROM READING HACKER NEWS I need-a dad.joke", new InsideABlockWithScoped ().fromString("{{#with dale}}{{tomdale ../need dad.joke}}{{/with}}").execute({ dale: {}, need: "need-a" }));
	}

	function testInformationAboutTypes () {
		var tomdale: HelperFunc = function (params, options) return switch (options.params) {
			case [Const("need"), Object("dad.joke"), Boolean(true), Boolean(false)]: 
				"Helper called";
      case _: throw "Invalid parameters: " + params;
		}

		Assert.same("Helper called", new Template ().fromString('{{tomdale "need" dad.joke true false}}').execute({ tomdale: tomdale }));
	}

	function testHashParameters () {
		var tomdale: HelperFunc = function (params, options) return switch (options.hashParams) {
			case [Const("need"), Object("dad.joke"), Boolean(true)]:
				"Helper called";
      case _: throw "Invalid parameters: " + params;
		}

		Assert.same("Helper called", new Template ().fromString('{{tomdale he.says desire="need" noun=dad.joke bool=true}}').execute({ tomdale: tomdale }));
	}

	// note: won't support this
	//function testHashParametersWithContext () {
	//}

	function testScopedAndBlockHelper () {
		Assert.same("STOP ME FROM READING HACKER NEWS I need-a dad.joke wot", new ScopedAndBlockHelper ().fromString("{{#with dale}}{{#tomdale ../need dad.joke}}wot{{/tomdale}}{{/with}}").execute({ dale: {}, need: "need-a" }));
	}
}

class InsideABlockWithScoped<T> extends Template<T> {
	override function localHelpers() return mergeHelpers({ 
		tomdale: function (params: Array<RType>, options: HelperOptions) {
			return switch (options.params) {
			case [Object(desire), Object(noun)]: 
				'STOP ME FROM READING HACKER NEWS I ${Reflect.field(options.ctx, desire)} ${noun}'; 
      case _: 
				throw "Invalid parameters: " + params;
			}
		},

		with: function (context: Array<RType>, options: HelperOptions) return switch (options.params) {
			case [Object(context)]: 
				options.fn(Reflect.field(options.ctx, context), {});
      case _: throw "Invalid parameters: " + context;
		}
	}, Helpers.core());
}

class ScopedAndBlockHelper<T> extends Template<T> {
	override function localHelpers() return mergeHelpers({ 
		tomdale: function (params: Array<RType>, options: HelperOptions) {
			return switch (options.params) {
				case [Object(desire), Object(noun)]: 
					'STOP ME FROM READING HACKER NEWS I ${Reflect.field(options.ctx, desire)} ${noun} ${options.fn(options.ctx, {})}'; 
				case _: 
					throw "Invalid parameters: " + params;
			}
		},

		with: function (context: Array<RType>, options: HelperOptions) return switch (options.params) {
			case [Object(context)]: 
				options.fn(Reflect.field(options.ctx, context), {});
      case _: throw "Invalid parameters: " + context;
		}
	}, Helpers.core());
}
