package test;

import stipple.Template;
import stipple.AST;
import utest.Assert;

class BuiltinTest {
	public function new () {}

	function testIf () {
		Assert.same("GOODBYE cruel world!", new Template ().fromString("{{#if goodbye}}GOODBYE {{/if}}cruel {{world}}!").execute({ goodbye: true, world: "world" }));
		Assert.same("GOODBYE cruel world!", new Template ().fromString("{{#if goodbye}}GOODBYE {{/if}}cruel {{world}}!").execute({ goodbye: "dummy", world: "world" }));
		Assert.same("cruel world!", new Template ().fromString("{{#if goodbye}}GOODBYE {{/if}}cruel {{world}}!").execute({ goodbye: false, world: "world" }));
		Assert.same("cruel world!", new Template ().fromString("{{#if goodbye}}GOODBYE {{/if}}cruel {{world}}!").execute({ world: "world" }));
		Assert.same("GOODBYE cruel world!", new Template ().fromString("{{#if goodbye}}GOODBYE {{/if}}cruel {{world}}!").execute({ goodbye: ['foo'], world: "world" }));
		Assert.same("cruel world!", new Template ().fromString("{{#if goodbye}}GOODBYE {{/if}}cruel {{world}}!").execute({ goodbye: [], world: "world" }));
		Assert.same("cruel world!", new Template ().fromString("{{#if goodbye}}GOODBYE {{/if}}cruel {{world}}!").execute({ goodbye: 0, world: "world" }));
	}

	function testIfWithFunctionArgument () {
		Assert.same("GOODBYE cruel world!", new Template ().fromString("{{#if goodbye}}GOODBYE {{/if}}cruel {{world}}!").execute({ goodbye: function (_, ?opts) { return true; }, world: "world" }));
		Assert.same("GOODBYE cruel world!", new Template ().fromString("{{#if goodbye}}GOODBYE {{/if}}cruel {{world}}!").execute({ goodbye: function (_, ?opts) return switch (_) { 
			case Object(o): return o.world; 
			case _: throw "Invalid object passed to inline helper: " + _;
		}, world: "world" }));
		Assert.same("cruel world!", new Template ().fromString("{{#if goodbye}}GOODBYE {{/if}}cruel {{world}}!").execute({ goodbye: function (_, ?opts) { return false; }, world: "world" }));
		Assert.same("cruel world!", new Template ().fromString("{{#if goodbye}}GOODBYE {{/if}}cruel {{world}}!").execute({ goodbye: function (_, ?opts) return switch (_) { 
			case Object(o): return o.foo; 
			case _: throw "Invalid object passed to inline helper: " + _;
		}, world: "world" }));
	}

	function testWith () {
		Assert.same("Alan Johnson", new Template ().fromString("{{#with person}}{{first}} {{last}}{{/with}}").execute({ person: { first: "Alan", last: "Johnson" }}));
	}

	function testWithWithFunctionArgument () {
		Assert.same("Alan Johnson", new Template ().fromString("{{#with person}}{{first}} {{last}}{{/with}}").execute({ person: function (_, ?opts) { return { first: "Alan", last: "Johnson" } } }));
	}

	function testEach () {
		Assert.same("goodbye! Goodbye! GOODBYE! cruel world!", new Template ().fromString("{{#each goodbyes}}{{text}}! {{/each}}cruel {{world}}!").execute({ goodbyes: [{text: "goodbye"}, {text: "Goodbye"}, {text: "GOODBYE"}], world: "world" }));
		Assert.same("cruel world!", new Template ().fromString("{{#each goodbyes}}{{text}}! {{/each}}cruel {{world}}!").execute({ goodbyes: [], world: "world" }));
	}

	function testEachWithAnObjectAndKey () {

		// NOTE: @each works differently than in handlebars on plain
		// objects, because haxe's reflection orders field names
		// differently on some targets, we sort field names
		// alphanumerically.
		
		Assert.same("&lt;b&gt;#1&lt;/b&gt;. goodbye! _2. GOODBYE! cruel world!", new Template ().fromString("{{#each goodbyes}}{{@key}}. {{text}}! {{/each}}cruel {{world}}!").execute({ goodbyes: {"<b>#1</b>": {text: "goodbye"}, "_2": {text: "GOODBYE"}}, world: "world"}));
		Assert.same("cruel world!", new Template ().fromString("{{#each goodbyes}}{{@key}}. {{text}}! {{/each}}cruel {{world}}!").execute({ goodbyes: [], world: "world"}));
	}

	function testEachWithIndex () {
		Assert.same("0. goodbye! 1. Goodbye! 2. GOODBYE! cruel world!", new Template ().fromString("{{#each goodbyes}}{{@index}}. {{text}}! {{/each}}cruel {{world}}!").execute({ goodbyes: [{ text: "goodbye" }, { text: "Goodbye" }, { text: "GOODBYE" }], world: "world" }));
	}

	function testEachWithNestedIndex () {
		Assert.same("0. goodbye! 0 1 2 After 0 1. Goodbye! 0 1 2 After 1 2. GOODBYE! 0 1 2 After 2 cruel world!", new Template ().fromString("{{#each goodbyes}}{{@index}}. {{text}}! {{#each ../goodbyes}}{{@index}} {{/each}}After {{@index}} {{/each}}{{@index}}cruel {{world}}!").execute({ goodbyes: [{ text: "goodbye" }, { text: "Goodbye" }, { text: "GOODBYE" }], world: "world" }));
	}

	function testEachObjectWithIndex () {
		Assert.same("0. goodbye! 1. Goodbye! 2. GOODBYE! cruel world!", new Template ().fromString("{{#each goodbyes}}{{@index}}. {{text}}! {{/each}}cruel {{world}}!").execute({ goodbyes: { "a": { text: "goodbye" }, b: { text: "Goodbye" }, c: { text: "GOODBYE" } }, world: "world" }));
	}

	function testEachWithFirst () {
		Assert.same("goodbye! cruel world!", new Template ().fromString("{{#each goodbyes}}{{#if @first}}{{text}}! {{/if}}{{/each}}cruel {{world}}!").execute({ goodbyes: [{ text: "goodbye" }, { text: "Goodbye" }, { text: "GOODBYE" }], world: "world" }));
	}

	function testEachWithNestedFirst () {
		Assert.same("(goodbye! goodbye! goodbye!) (goodbye!) (goodbye!) cruel world!", new Template ().fromString("{{#each goodbyes}}({{#if @first}}{{text}}! {{/if}}{{#each ../goodbyes}}{{#if @first}}{{text}}!{{/if}}{{/each}}{{#if @first}} {{text}}!{{/if}}) {{/each}}cruel {{world}}!").execute({ goodbyes: [{ text: "goodbye" }, { text: "Goodbye" }, { text: "GOODBYE" }], world: "world" }));
	}

	function testEachObjectWithFirst () {

		// NOTE: @first works differently than in handlebars on plain
		// objects, because haxe's reflection orders field names
		// differently on some targets, we sort field names
		// alphanumerically and take the first.
		
		Assert.same("goodbye! cruel world!", new Template ().fromString("{{#each goodbyes}}{{#if @first}}{{text}}! {{/if}}{{/each}}cruel {{world}}!").execute({ goodbyes: { "foo": { text: "goodbye" }, zoo: { text: "Goodbye" } }, world: "world" }));
	}

	function testEachWithLast () {
		Assert.same("GOODBYE! cruel world!", new Template ().fromString("{{#each goodbyes}}{{#if @last}}{{text}}! {{/if}}{{/each}}cruel {{world}}!").execute({ goodbyes: [{ text: "goodbye" }, { text: "Goodbye" }, { text: "GOODBYE" }], world: "world" }));
	}

	function testEachWithNestedLast () {
		Assert.same("(GOODBYE!) (GOODBYE!) (GOODBYE! GOODBYE! GOODBYE!) cruel world!", new Template ().fromString("{{#each goodbyes}}({{#if @last}}{{text}}! {{/if}}{{#each ../goodbyes}}{{#if @last}}{{text}}!{{/if}}{{/each}}{{#if @last}} {{text}}!{{/if}}) {{/each}}cruel {{world}}!").execute({ goodbyes: [{ text: "goodbye" }, { text: "Goodbye" }, { text: "GOODBYE" }], world: "world" }));
	}

	function testEachWithFunctionArgument () {
		Assert.same("goodbye! Goodbye! GOODBYE! cruel world!", new Template ().fromString("{{#each goodbyes}}{{text}}! {{/each}}cruel {{world}}!").execute({ goodbyes: function (_, ?opts) return [{text: "goodbye" }, { text: "Goodbye" }, { text: "GOODBYE" }], world: "world" }));
		Assert.same("cruel world!", new Template ().fromString("{{#each goodbyes}}{{text}}! {{/each}}cruel {{world}}!").execute({ goodbyes: [], world: "world" }));
	}

	/**
	 * DEPRECATED
	function testDataPassedToHelpers () {
		var helpers = new Map();
		helpers.set("detectDataInsideEach", function (_, opts: HelperOptions): String return if (opts != null && opts.data != null) opts.data.exclaim else "");
		Assert.same("a!b!c!", new Template (helpers).fromString("{{#each letters}}{{this}}{{detectDataInsideEach}}{{/each}}").execute({ letters: ["a", "b", "c" ]}, { exclaim: "!" }));
	}
	*/
}
