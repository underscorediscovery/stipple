package test;

import stipple.AST;
import stipple.Lexer;

import utest.Assert;

typedef ParserToken = Array<{name: String, value: String}>;

class TokenizerTest {
	public function new () {}

	function testTokenizesASimpleMustache () {
		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{foo}}")).parse());
		Assert.same(["OPEN", "ID", "CLOSE"], result[0].map(function (_) return _.name));
	}

	function testUnescapingWithAmpersand () {
		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{&bar}}")).parse());
		Assert.same(["OPEN", "ID", "CLOSE"], result[0].map(function (_) return _.name));

		Assert.same({ name: "OPEN", value: "{{&", pos: 0 }, result[0][0]);
		Assert.same({ name: "ID", value: "bar", pos: 3 }, result[0][1]);
	}

	function testUnescapingWithTripleCurlyBrackets () {
		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{{bar}}}")).parse());
		Assert.same(["OPEN_UNESCAPED", "ID", "CLOSE_UNESCAPED"], result[0].map(function (_) return _.name));

		Assert.same({ name: "ID", value: "bar", pos: 3 }, result[0][1]);
	}

	function testEscapingDelimiters () {
		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{foo}} \\{{bar}} {{baz}}")).parse());
		Assert.same(["OPEN", "ID", "CLOSE"], result[0].map(function (_) return _.name));
		Assert.same({name: "INNERCONTENT", value: "\\{{bar}} ", pos: 8 }, result[2]);
		Assert.same(["OPEN", "ID", "CLOSE"], result[3].map(function (_) return _.name));
	}

	function testSingleCurlyBrackets () {
		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("<script> var configMap = { }; </script>")).parse());
		Assert.same({name: "INNERCONTENT", value: "<script> var configMap = { ", pos: 0}, result[0]);
		Assert.same({name: "INNERCONTENT", value: "}; </script>", pos: 27}, result[1]);

	}

	function testEscapingMultipleDelimiters () {
		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{foo}} \\{{bar}} \\{{baz}}")).parse());
		Assert.same(["OPEN", "ID", "CLOSE"], result[0].map(function (_) return _.name));
		Assert.same({name: "INNERCONTENT", value: "\\{{bar}} ", pos: 8 }, result[2]);
		Assert.same({name: "INNERCONTENT", value: "\\{{baz}}", pos: 17 }, result[3]);
	}

	function testEscapingATripleStash () {
		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{foo}} \\{{{bar}}} {{baz}}")).parse());
		Assert.same(["OPEN", "ID", "CLOSE"], result[0].map(function (_) return _.name));
		Assert.same("INNERCONTENT", result[1].name);
		Assert.same("INNERCONTENT", result[2].name);
		Assert.same(["OPEN", "ID", "CLOSE"], result[3].map(function (_) return _.name));
	}

	function testEscapingEscapeCharacter () {
		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{foo}} \\\\{{bar}} {{baz}}")).parse());
		Assert.same(["OPEN", "ID", "CLOSE"], result[0].map(function (_) return _.name));
		Assert.same({name: "INNERCONTENT", value: "\\\\", pos: 8}, result[2]);
		Assert.same(["OPEN", "ID", "CLOSE"], result[3].map(function (_) return _.name));
		Assert.same({name: "ID", value: "bar", pos: 12}, result[3][1]);
	}

	function testEscapingMultipleEscapeCharacters () {
		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{foo}} \\\\{{bar}} \\\\{{baz}}")).parse());
		Assert.same(["OPEN", "ID", "CLOSE"], result[0].map(function (_) return _.name));
		Assert.same({name: "INNERCONTENT", value: "\\\\", pos: 8 }, result[2]);
		Assert.same(["OPEN", "ID", "CLOSE"], result[3].map(function (_) return _.name));
		Assert.same({name: "ID", value: "bar", pos: 12}, result[3][1]);
		Assert.same({name: "INNERCONTENT", value: "\\\\", pos: 18 }, result[5]);
		Assert.same(["OPEN", "ID", "CLOSE"], result[6].map(function (_) return _.name));
		Assert.same({name: "ID", value: "baz", pos: 22}, result[6][1]);
	};

	function testMixedEscapedDelimitersAndEscapedEscapeCharacters () {
		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{foo}} \\\\{{bar}} \\{{baz}}")).parse());
		Assert.same(["OPEN", "ID", "CLOSE"], result[0].map(function (_) return _.name));
		Assert.same({name: "INNERCONTENT", value: "\\\\", pos: 8 }, result[2]);
		Assert.same({name: "OPEN", value: "{{", pos: 10 }, result[3][0]);
		Assert.same({name: "ID", value: "bar", pos: 12 }, result[3][1]);
		Assert.same(["OPEN", "ID", "CLOSE"], result[3].map(function (_) return _.name));
		Assert.same({name: "INNERCONTENT", value: "\\{{baz}}", pos: 18 }, result[5]);
	}

	function testEscapedEscapeCharacterOnATripleStash () {
		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{foo}} \\\\{{{bar}}} {{baz}}")).parse());
		Assert.same(["OPEN", "ID", "CLOSE"], result[0].map(function (_) return _.name));
		Assert.same({name: "INNERCONTENT", value: "\\\\", pos: 8 }, result[2]);
		Assert.same(["OPEN_UNESCAPED", "ID", "CLOSE_UNESCAPED"], result[3].map(function (_) return _.name));
		Assert.same({name: "ID", value: "bar", pos: 13 }, result[3][1]);
		Assert.same({name: "INNERCONTENT", value: " ", pos: 19 }, result[4]);
		Assert.same(["OPEN", "ID", "CLOSE"], result[5].map(function (_) return _.name));
	}

	function testTokenizesASimplePath () {
		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{foo/bar}}")).parse());
		Assert.same("OPEN", result[0][0].name);
		Assert.same("ID", result[0][1].name);
		Assert.same(["SEP", "ID"], result[0][2].map(function (_) return _.name));
		Assert.same("CLOSE", result[0][3].name);
	}

	function testAllowsDotNotation () {
		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{foo.bar}}")).parse());
		Assert.same("OPEN", result[0][0].name);
		Assert.same("ID", result[0][1].name);
		Assert.same(["SEP", "ID"], result[0][2].map(function (_) return _.name));
		Assert.same("CLOSE", result[0][3].name);

		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{foo.bar.baz}}")).parse());
		Assert.same("OPEN", result[0][0].name);
		Assert.same("ID", result[0][1].name);
		Assert.same(["SEP", "ID"], result[0][2].map(function (_) return _.name));
		Assert.same(["SEP", "ID"], result[0][3].map(function (_) return _.name));
		Assert.same("CLOSE", result[0][4].name);

		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{foo.bar.baz.zap}}")).parse());
		Assert.same("OPEN", result[0][0].name);
		Assert.same("ID", result[0][1].name);
		Assert.same(["SEP", "ID"], result[0][2].map(function (_) return _.name));
		Assert.same(["SEP", "ID"], result[0][3].map(function (_) return _.name));
		Assert.same(["SEP", "ID"], result[0][4].map(function (_) return _.name));
		Assert.same("CLOSE", result[0][5].name);
	}

	function testAllowsPathLiterals () {
		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{foo.[bar]}}")).parse());
		Assert.same("OPEN", result[0][0].name);
		Assert.same("ID", result[0][1].name);
		Assert.same(["SEP", "ID"], result[0][2].map(function (_) return _.name));
		Assert.same("CLOSE", result[0][3].name);
	}

	function testAllowsMultiplePathLiteralsOnALine () {
		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{foo.[bar]}}{{foo.[baz]}}")).parse());

		Assert.same("OPEN", result[0][0].name);
		Assert.same("ID", result[0][1].name);
		Assert.same(["SEP", "ID"], result[0][2].map(function (_) return _.name));
		Assert.same("CLOSE", result[0][3].name);

		Assert.same("OPEN", result[1][0].name);
		Assert.same("ID", result[1][1].name);
		Assert.same(["SEP", "ID"], result[1][2].map(function (_) return _.name));
		Assert.same("CLOSE", result[1][3].name);
	}

	function testTokenizesDot () {
		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{.}}")).parse());
		Assert.same(["OPEN", "ID", "CLOSE"], result[0].map(function (_) return _.name));
	}

	function testTokenizesAPath () {
		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{../foo/bar}}")).parse());
		Assert.same("OPEN", result[0][0].name);
		Assert.same("ID", result[0][1].name);
		Assert.same(["SEP", "ID"], result[0][2].map(function (_) return _.name));
		Assert.same(["SEP", "ID"], result[0][3].map(function (_) return _.name));
		Assert.same("CLOSE", result[0][4].name);
		Assert.same({ name: "ID", value: "..", pos: 2 }, result[0][1]);
	}

	function testTokenizesAParentPath () {
		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{../foo.bar}}")).parse());
		Assert.same("OPEN", result[0][0].name);
		Assert.same("ID", result[0][1].name);
		Assert.same(["SEP", "ID"], result[0][2].map(function (_) return _.name));
		Assert.same(["SEP", "ID"], result[0][3].map(function (_) return _.name));
		Assert.same("CLOSE", result[0][4].name);
		Assert.same({ name: "ID", value: "..", pos: 2 }, result[0][1]);
	}

	function testTokenizesARelativePath () {
		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{this/foo}}")).parse());
		Assert.same("OPEN", result[0][0].name);
		Assert.same("ID", result[0][1].name);
		Assert.same(["SEP", "ID"], result[0][2].map(function (_) return _.name));
		Assert.same("CLOSE", result[0][3].name);
		Assert.same({ name: "ID", value: "this", pos: 2 }, result[0][1]);
		Assert.same({ name: "ID", value: "foo", pos: 7 }, result[0][2][1]);
	}

	function testTokenizesWithSpaces () {
		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{  foo  }}")).parse());
		Assert.same(["OPEN", "ID", "CLOSE"], result[0].map(function (_) return _.name));
		Assert.same({ name: "ID", value: "foo", pos: 4 }, result[0][1]);
	}

	function testTokenizesWithLineBreaks () {
		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{  foo  \n  bar  }}")).parse());
		Assert.same("OPEN", result[0][0].name);
		Assert.same("ID", result[0][1].name);
		Assert.same(["ID"], result[0][2].map(function (_) return _.name));
		Assert.same("CLOSE", result[0][3].name);
		Assert.same({ name: "ID", value: "foo", pos: 4 }, result[0][1]);
		Assert.same({ name: "ID", value: "bar", pos: 12 }, result[0][2][0]);
	}

	function testTokenizesRawContent () {
		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("foo {{ bar }} baz")).parse());
		Assert.same({ name: "INNERCONTENT", value: "foo ", pos: 0 }, result[0]);
		Assert.same(["OPEN", "ID", "CLOSE"], result[1].map(function (_) return _.name));
		Assert.same({ name: "INNERCONTENT", value: " baz", pos: 13 }, result[2]);
	}

	function testTokenizesAPartial () {
		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{> foo}}")).parse());
		Assert.same(["OPEN_PARTIAL", "ID", "CLOSE"], result[0].map(function (_) return _.name));
	}

	function testTokenizesAPartialWithContext () {
		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{> foo bar }}")).parse());
		Assert.same(["OPEN_PARTIAL", "ID", "ID", "CLOSE"], result[0].map(function (_) return _.name));
	}

	function testTokenizesAPartialWithoutSpaces () {
		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{>foo}}")).parse());
		Assert.same(["OPEN_PARTIAL", "ID", "CLOSE"], result[0].map(function (_) return _.name));
	}

	function testTokenizesAPartialWithSpace () {
		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{>foo  }}")).parse());
		Assert.same(["OPEN_PARTIAL", "ID", "CLOSE"], result[0].map(function (_) return _.name));
	}

	function testTokenizesAPartialWithSpaceAndNestedPath () {
		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{>foo/bar.baz  }}")).parse());
		Assert.same("OPEN_PARTIAL", result[0][0].name);
		Assert.same("ID", result[0][1].name);
		Assert.same(["SEP", "ID"], result[0][2].map(function (_) return _.name));
		Assert.same(["SEP", "ID"], result[0][3].map(function (_) return _.name));
		Assert.same("CLOSE", result[0][4].name);
	}

	function testTokenizesAComment () {
		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("foo {{! this is a comment }} bar {{ baz }}")).parse());
		Assert.same("INNERCONTENT", result[0].name);
		Assert.same({name: "COMMENT", value: " this is a comment ", pos: 7}, result[1]);
		Assert.same("INNERCONTENT", result[2].name);
		Assert.same(["OPEN", "ID", "CLOSE"], result[3].map(function (_) return _.name));
	}

	function testTokenizesABlockComment () {
		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("foo {{!-- this is a {{comment}} --}} bar {{ baz }}")).parse());
		Assert.same("INNERCONTENT", result[0].name);
		Assert.same({name: "COMMENT", value: " this is a {{comment}} ", pos: 9}, result[1]);
		Assert.same("INNERCONTENT", result[2].name);
		Assert.same(["OPEN", "ID", "CLOSE"], result[3].map(function (_) return _.name));
	}

	function testTokenizesABlockCommentWithWhitespace () {
		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("foo {{!-- this is a \n{{comment}}\n--}} bar {{ baz }}")).parse());
		Assert.same("INNERCONTENT", result[0].name);
		Assert.same({name: "COMMENT", value: " this is a \n{{comment}}\n", pos: 9}, result[1]);
		Assert.same("INNERCONTENT", result[2].name);
		Assert.same(["OPEN", "ID", "CLOSE"], result[3].map(function (_) return _.name));
	}

	function testTokenizesOpenAndClosingBlocks () {
		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{#foo}}content{{/foo}}")).parse());
		Assert.same(["OPEN_BLOCK", "ID", "CLOSE", "STATEMENTS", "OPEN_ENDBLOCK", "ID", "CLOSE"], result[0].map(function (_) return _.name));
		Assert.same("INNERCONTENT", result[0][3].value[0].name);
	}

	function testTokenizesInverseSections () {
		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{#foo}}{{^}}{{/foo}}")).parse());
		Assert.same(["OPEN_BLOCK", "ID", "CLOSE", "STATEMENTS", "OPEN_ENDBLOCK", "ID", "CLOSE"], result[0].map(function (_) return _.name));

		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{#foo}} bar {{^}}{{/foo}}")).parse());
		Assert.same(["OPEN_BLOCK", "ID", "CLOSE", "STATEMENTS", "OPEN_ENDBLOCK", "ID", "CLOSE"], result[0].map(function (_) return _.name));

		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{^foo}}bar{{/foo}}")).parse());
		Assert.same(["OPEN_INVERSE", "ID", "CLOSE", "STATEMENTS", "OPEN_ENDBLOCK", "ID", "CLOSE"], result[0].map(function (_) return _.name));
	}

	function testTokenizesInverseSectionsWithSpaces () {
		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{^ foo  }}{{/foo}}")).parse());
		Assert.same(["OPEN_INVERSE", "ID", "CLOSE", "OPEN_ENDBLOCK", "ID", "CLOSE"], result[0].map(function (_) return _.name));
	}

	function testTokenizesMustachesWithParams () {
		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{ foo bar baz }}")).parse());
		Assert.same("OPEN", result[0][0].name);
		Assert.same({name: "ID", value: "foo", pos: 3}, result[0][1]);
		Assert.same({name: "ID", value: "bar", pos: 7}, result[0][2][0]);
		Assert.same({name: "ID", value: "baz", pos: 11}, result[0][3][0]);
		Assert.same("CLOSE", result[0][4].name);
	}

	function testTokenizesMustachesWithDoubleQuotes () {
		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{ foo bar \"baz\" }}")).parse());
		Assert.same("OPEN", result[0][0].name);
		Assert.same({name: "ID", value: "foo", pos: 3}, result[0][1]);
		Assert.same({name: "ID", value: "bar", pos: 7}, result[0][2][0]);
		Assert.same({name: "STRINGLITERAL", value: "\"baz\"", pos: 11}, result[0][3]);
		Assert.same("CLOSE", result[0][4].name);
	}

	function testTokenizesMustachesWithSingleQuotes () {
		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{ foo bar 'baz' }}")).parse());
		Assert.same("OPEN", result[0][0].name);
		Assert.same({name: "ID", value: "foo", pos: 3}, result[0][1]);
		Assert.same({name: "ID", value: "bar", pos: 7}, result[0][2][0]);
		Assert.same({name: "STRINGLITERAL", value: "'baz'", pos: 11}, result[0][3]);
		Assert.same("CLOSE", result[0][4].name);
	}

	function testTokenizesStringParamsWithSpaces () {
		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{ foo bar \"baz bat\" }}")).parse());
		Assert.same("OPEN", result[0][0].name);
		Assert.same({name: "ID", value: "foo", pos: 3}, result[0][1]);
		Assert.same({name: "ID", value: "bar", pos: 7}, result[0][2][0]);
		Assert.same({name: "STRINGLITERAL", value: "\"baz bat\"", pos: 11}, result[0][3]);
		Assert.same("CLOSE", result[0][4].name);
	}

	function testTokenizesStringParamsWithEscapedQuotes () {
		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString('{{ foo "bar\\"baz" }}')).parse());
		Assert.same("OPEN", result[0][0].name);
		Assert.same({name: "ID", value: "foo", pos: 3}, result[0][1]);
		Assert.same({name: "STRINGLITERAL", value: '"bar\\"baz"', pos: 7}, result[0][2]);
		Assert.same("CLOSE", result[0][3].name);
	}

	function testTokenizesStringParamsUsingSingleQuotesWithEscapedQuotes () {
		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{ foo 'bar\\'baz' }}")).parse());
		Assert.same("OPEN", result[0][0].name);
		Assert.same({name: "ID", value: "foo", pos: 3}, result[0][1]);
		Assert.same({name: "STRINGLITERAL", value: "'bar\\'baz'", pos: 7}, result[0][2]);
		Assert.same("CLOSE", result[0][3].name);
	}

	function testTokenizesNumbers () {
		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{ foo 1 }}")).parse());
		Assert.same(["OPEN", "ID", "INTEGER", "CLOSE"], result[0].map(function (_) return _.name));
		Assert.same({name: "INTEGER", value: "1", pos: 7}, result[0][2]);

		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{ foo -1 }}")).parse());
		Assert.same(["OPEN", "ID", "INTEGER", "CLOSE"], result[0].map(function (_) return _.name));
		Assert.same({name: "INTEGER", value: "-1", pos: 7}, result[0][2]);
	}

	function testTokenizesBooleans () {
		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{foo true }}")).parse());
		Assert.same("OPEN", result[0][0].name);
		Assert.same({name: "ID", value: "foo", pos: 2}, result[0][1]);
		Assert.same({name: "BOOLEAN", value: "true", pos: 6}, result[0][2]);
		Assert.same("CLOSE", result[0][3].name);

		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{ foo false }}")).parse());
		Assert.same("OPEN", result[0][0].name);
		Assert.same({name: "ID", value: "foo", pos: 3}, result[0][1]);
		Assert.same({name: "BOOLEAN", value: "false", pos: 7}, result[0][2]);
		Assert.same("CLOSE", result[0][3].name);
	}

	function testTokenizesHashArguments () {
		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{ foo bar=baz }}")).parse());
		Assert.same("OPEN", result[0][0].name);
		Assert.same({name: "ID", value: "foo", pos: 3}, result[0][1]);
		Assert.same(["ID", "EQUALS", "ID"], result[0][2].map(function(_) return _.name));
		Assert.same("CLOSE", result[0][3].name);

		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{ foo bar baz=bat }}")).parse());
		Assert.same("OPEN", result[0][0].name);
		Assert.same({name: "ID", value: "foo", pos: 3}, result[0][1]);
		Assert.same({name: "ID", value: "bar", pos: 7}, result[0][2][0]);
		Assert.same(["ID", "EQUALS", "ID"], result[0][3].map(function(_) return _.name));
		Assert.same("CLOSE", result[0][4].name);

		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{ foo bar baz=1 }}")).parse());
		Assert.same("OPEN", result[0][0].name);
		Assert.same({name: "ID", value: "foo", pos: 3}, result[0][1]);
		Assert.same({name: "ID", value: "bar", pos: 7}, result[0][2][0]);
		Assert.same(["ID", "EQUALS", "INTEGER"], result[0][3].map(function(_) return _.name));
		Assert.same("CLOSE", result[0][4].name);

		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{ foo bar baz=true }}")).parse());
		Assert.same("OPEN", result[0][0].name);
		Assert.same({name: "ID", value: "foo", pos: 3}, result[0][1]);
		Assert.same({name: "ID", value: "bar", pos: 7}, result[0][2][0]);
		Assert.same(["ID", "EQUALS", "BOOLEAN"], result[0][3].map(function(_) return _.name));
		Assert.same("CLOSE", result[0][4].name);

		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{ foo bar baz=false }}")).parse());
		Assert.same("OPEN", result[0][0].name);
		Assert.same({name: "ID", value: "foo", pos: 3}, result[0][1]);
		Assert.same({name: "ID", value: "bar", pos: 7}, result[0][2][0]);
		Assert.same(["ID", "EQUALS", "BOOLEAN"], result[0][3].map(function(_) return _.name));
		Assert.same("CLOSE", result[0][4].name);

		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{ foo bar\n  baz=bat }}")).parse());
		Assert.same("OPEN", result[0][0].name);
		Assert.same({name: "ID", value: "foo", pos: 3}, result[0][1]);
		Assert.same({name: "ID", value: "bar", pos: 7}, result[0][2][0]);
		Assert.same(["ID", "EQUALS", "ID"], result[0][3].map(function(_) return _.name));
		Assert.same("CLOSE", result[0][4].name);

		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{ foo bar baz=\"bat\" }}")).parse());
		Assert.same("OPEN", result[0][0].name);
		Assert.same({name: "ID", value: "foo", pos: 3}, result[0][1]);
		Assert.same({name: "ID", value: "bar", pos: 7}, result[0][2][0]);
		Assert.same(["ID", "EQUALS", "STRINGLITERAL"], result[0][3].map(function(_) return _.name));
		Assert.same("CLOSE", result[0][4].name);

		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{ foo bar baz=\"bat\" bam=wot }}")).parse());
		Assert.same("OPEN", result[0][0].name);
		Assert.same({name: "ID", value: "foo", pos: 3}, result[0][1]);
		Assert.same({name: "ID", value: "bar", pos: 7}, result[0][2][0]);
		Assert.same(["ID", "EQUALS", "STRINGLITERAL"], result[0][3].map(function(_) return _.name));
		Assert.same(["ID", "EQUALS", "ID"], result[0][4].map(function(_) return _.name));
		Assert.same("CLOSE", result[0][5].name);

		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{foo omg bar=baz bat=\"bam\"}}")).parse());
		Assert.same("OPEN", result[0][0].name);
		Assert.same({name: "ID", value: "foo", pos: 2}, result[0][1]);
		Assert.same({name: "ID", value: "omg", pos: 6}, result[0][2][0]);
		Assert.same(["ID", "EQUALS", "ID"], result[0][3].map(function(_) return _.name));
		Assert.same(["ID", "EQUALS", "STRINGLITERAL"], result[0][4].map(function(_) return _.name));
		Assert.same("CLOSE", result[0][5].name);

		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{foo bar=baz bat=bam}}")).parse());
		Assert.same("OPEN", result[0][0].name);
		Assert.same({name: "ID", value: "foo", pos: 2}, result[0][1]);
		Assert.same(["ID", "EQUALS", "ID"], result[0][2].map(function(_) return _.name));
		Assert.same(["ID", "EQUALS", "ID"], result[0][3].map(function(_) return _.name));
		Assert.same("CLOSE", result[0][4].name);
	}

	function testTokenizesSpecialIdentifiers () {
		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{ @foo }}")).parse());
		Assert.same(["OPEN", "DATA", "ID", "CLOSE"], result[0].map(function(_) return _.name));
		Assert.same({name: "ID", value: "foo", pos: 4}, result[0][2]);

		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{ foo @bar }}")).parse());
		Assert.same("OPEN", result[0][0].name);
		Assert.same({name: "ID", value: "foo", pos: 3}, result[0][1]);
		Assert.same(["DATA", "ID"], result[0][2].map(function(_) return _.name));
		Assert.same("CLOSE", result[0][3].name);

		var result: Array<Dynamic> = AST.dump(new HpsParser(byte.ByteData.ofString("{{ foo bar=@baz }}")).parse());
		Assert.same("OPEN", result[0][0].name);
		Assert.same({name: "ID", value: "foo", pos: 3}, result[0][1]);
		Assert.same(["ID", "EQUALS", "DATA", "ID"], result[0][2].map(function(_) return _.name));
		Assert.same("CLOSE", result[0][3].name);
	}

	function testDoesNotTimeOutWithInvalidBlockSyntax () {
		Assert.raises(function () AST.dump(new HpsParser(byte.ByteData.ofString("{{foo}")).parse()), hxparse.UnexpectedChar);
	}

	function testDoesNotTimeOutWithInvalidIDCharacters () {
		Assert.raises(function () AST.dump(new HpsParser(byte.ByteData.ofString("{{foo & }}")).parse()), hxparse.UnexpectedChar);
	}
}
