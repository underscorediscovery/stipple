package test;

using Lambda;

import utest.Assert;

using stipple.Template;
import stipple.AST;
import stipple.Helpers;
import stipple.Lexer;

class DataTest {
	public function new () {}

	function testPassingInDataToACompiledFunction () {
		Assert.same("happy cat", new PassingInDataToACompiledFunction ().fromString("{{#dice}}{{hello}}{{/dice}}").execute({ noun: "cat" }));
	}

	function testDataCanBeLookedUp () {
		Assert.same("hello", new DataCanBeLookedUp ().fromString("{{#dice}}{{@hello}}{{/dice}}").execute({}));
	}

	function testDataTriggersAutomaticTopLevelLookup () {

		var let: HelperFunc = function (_, options) {

			var frame = Reflect.copy(options.data);
			Reflect.setField(frame, "world", [options.hash.world].extract());

			return options.fn(options.ctx, frame);
		}

		Assert.same("Hello world", new Template ().fromString('{{#let world="world"}}{{#if foo}}{{#if foo}}Hello {{@world}}{{/if}}{{/if}}{{/let}}').execute({foo: true, let: let }));
	}

	function testParameterDataCanBeLookedUp () {

		var hello: HelperFunc = function (_, options) return switch (_) {
			case [Const(noun)]: 'Hello ${noun}';
			case _: throw "Couldn't find 'noun' on context: " + _;
		}

		Assert.same("Hello world", new ParameterDataCanBeLookedUp ().fromString("{{#dice}}{{hello @world}}{{/dice}}").execute({ hello: hello }));
	}

	function testNestedParameterDataCanBeLookedUpViaPath () {
		var hello: HelperFunc = function (_, options) return switch (_) {
			case [Const(noun)]: 'Hello ${noun}';
			case _: throw "Couldn't find 'noun' on context: " + _;
		}

		Assert.same("Hello world", new NestedParameterDataCanBeLookedUpViaPath ().fromString("{{#dice}}{{hello @world.bar}}{{/dice}}").execute({ hello: hello }));
	}

	function testNestedParameterDataDoesNotFailWhenMissing () {
		var hello: HelperFunc = function (_, options) return switch (_) {
			case [Const(noun)]: 'Hello ${noun}';
			case _: 'Hello moon';
		}

		Assert.same("Hello moon", new NestedParameterDataDoesNotFailWhenMissing ().fromString("{{#dice}}{{hello @world.bar}}{{/dice}}").execute({ hello: hello }));
	}

	function testParameterDataThrowsWhenUsingThisScopeReferences () {
		Assert.raises(function () new Template ().fromString("{{#goodbyes}}{{text}} cruel {{@./name}}! {{/goodbyes}}").execute({ goodbyes: true }), String);
	}

	function testParameterDataThrowsWhenUsingParentScopeReferences () {
		Assert.raises(function () new Template ().fromString("{{#goodbyes}}{{text}} cruel {{@../name}}! {{/goodbyes}}").execute({ goodbyes: true }), String);
	}

	function testParameterDataThrowsWhenUsingComplexScopeReferences () {
		Assert.raises(function () AST.parse(new HpsParser(byte.ByteData.ofString("{{#goodbyes}}{{text}} cruel {{@foo/../name}}! {{/goodbyes}}")).parse()), String);
	}

	function testDataIsInheritedDownstream () {

		var let: HelperFunc = function (_, options) {

			var frame = Reflect.fields(options.hash).fold(function (l, rest: Dynamic) return switch (cast(Reflect.field(options.hash, l), RType)) {
				case Const(w): Reflect.setField(rest, l, w); rest;
				case _: rest;
			}, Reflect.copy(options.data));

			return options.fn(options.ctx, frame);
		}

		Assert.same("2hello world1", new Template ().fromString("{{#let foo=1 bar=2}}{{#let foo=bar.baz}}{{@bar}}{{@foo}}{{/let}}{{@foo}}{{/let}}").execute({ bar: { baz: "hello world" }, let: let }));
	}

	function testPassingInDataToACompiledFunctionWorksWithHelpersInPartials () {

		Assert.same("happy cat", {
			var tpl = new PassingInDataToACompiledFunctionWorksWithHelpersInPartials();
			tpl.fromString("{{#dice}}{{>myPartial}}{{/dice}}");
			tpl.execute({ noun: "cat" });
		});
	}

	function testPassingInDataToACompiledFunctionWorksWithHelpersAndParameters () {
		var hello: HelperFunc = function (_, options) return switch (_) {
			case [Const(noun)]: options.data.adjective + " " + noun + (options.ctx.exclaim ? "!" : "");
			case _: throw "Invalid context: " + _;
		}

		Assert.same("happy world!", new PassingInDataToACompiledFunctionWorksWithHelpersAndParameters().fromString("{{#dice}}{{hello world}}{{/dice}}").execute({exclaim: true, world: "world", hello: hello}));
	}

	function testPassingInDataWorksWithBlockHelpers () {
		Assert.same("happy world!", new PassingInDataWorksWithBlockHelpers ().fromString("{{#hello}}{{world}}{{/hello}}").execute({exclaim: true}));
	}

	function testPassingInDataWorksWithBlockHelpersThatUseParentPath () {
		var hello: HelperFunc = function (_, options) return options.fn({ exclaim: "?" }, { adjective: "happy" });

		Assert.same("happy world?", new PassingInDataWorksWithBlockHelpersThatUseParentPath ().fromString("{{#hello}}{{world ../zomg}}{{/hello}}").execute({exclaim: true, zomg: "world", hello: hello } ));

	}

	function testPassingInDataIsPassedToBlockHelpersWhereChildrenUseParentPath () {
		var hello: HelperFunc = function (_, options) return options.data.accessData + " " + options.fn({ exclaim: "?" }, options.data);

		Assert.same("#win happy world?", new PassingInDataIsPassedToBlockHelpersWhereChildrenUseParentPath ().fromString("{{#dice}}{{#hello}}{{world ../zomg}}{{/hello}}{{/dice}}").execute({exclaim: true, zomg: "world", hello: hello}));
	}

}

class PassingInDataToACompiledFunction<T> extends Template<T> {
	override function localHelpers() return mergeHelpers({
		dice: function (_: Array<RType>, options: HelperOptions) return options.fn(options.ctx, { adjective: "happy" }),
		hello: function(_: Array<RType>, options: HelperOptions) return switch (_) {
			case [Object(o)]: options.data.adjective + " " + o.noun;
			case _: throw 'invalid object returned to helper: ${_}';
		}
	}, Helpers.core());
}

class PassingInDataWorksWithBlockHelpers<T> extends Template<T> {
	override function localHelpers() return mergeHelpers({
		hello: function (_: Array<RType>, options: HelperOptions) return options.fn(options.ctx, { adjective: "happy" }),
		world: function (_: Array<RType>, options: HelperOptions) return options.data.adjective + " world" + (options.ctx.exclaim ? "!" : "")
	}, Helpers.core());
}

class DataCanBeLookedUp<T> extends Template<T> {
	override function localHelpers() return mergeHelpers({
		dice: function (_: Array<RType>, options: HelperOptions) return options.fn(options.ctx, { hello: "hello" }),
	}, Helpers.core());
}

class ParameterDataCanBeLookedUp<T> extends Template<T> {
	override function localHelpers() return mergeHelpers({
		dice: function (_: Array<RType>, options: HelperOptions) return options.fn(options.ctx, { world: "world" }),
	}, Helpers.core());
}

class NestedParameterDataCanBeLookedUpViaPath<T> extends Template<T> {
	override function localHelpers() return mergeHelpers({
		dice: function (_: Array<RType>, options: HelperOptions) return options.fn(options.ctx, { world: { bar: "world" } }),
	}, Helpers.core());
}

class NestedParameterDataDoesNotFailWhenMissing<T> extends Template<T> {
	override function localHelpers() return mergeHelpers({
		dice: function (_: Array<RType>, options: HelperOptions) return options.fn(options.ctx, { foo: { bar: "world" } }),
	}, Helpers.core());
}

class PassingInDataToACompiledFunctionWorksWithHelpersInPartials<T> extends Template<T> {
	override function localPartials(): Dynamic<Template<T>> return { myPartial: new Template ().fromString("{{hello}}") };
	override function localHelpers() return mergeHelpers({
		dice: function (_: Array<RType>, options: HelperOptions) return options.fn(options.ctx, { adjective: "happy" }),
		hello: function (_: Array<RType>, options: HelperOptions) return switch (_) {
			case [Object(o)]: options.data.adjective + " " + o.noun;
			case _: throw "Invalid context: " + _;
		}
	}, Helpers.core());
}

class PassingInDataToACompiledFunctionWorksWithHelpersAndParameters<T> extends Template<T> {
	override function localHelpers() return mergeHelpers({
		dice: function (_: Array<RType>, options: HelperOptions) return options.fn(options.ctx, { adjective: "happy" }),
	}, Helpers.core());
}

class PassingInDataWorksWithBlockHelpersThatUseParentPath<T> extends Template<T> {
	override function localHelpers() return mergeHelpers({
		world: function (_: Array<RType>, options: HelperOptions) return switch (_) {
			case [Const(thing)]: options.data.adjective + " " + thing + (options.ctx.exclaim != null ? options.ctx.exclaim : "");
			case _: throw "Invalid context: " + _;
		} 
	}, Helpers.core());
}

class PassingInDataIsPassedToBlockHelpersWhereChildrenUseParentPath<T> extends Template<T> {
	override function localHelpers() return mergeHelpers({
		dice: function (_: Array<RType>, options: HelperOptions) return options.fn(options.ctx, { adjective: "happy", accessData: "#win" }),
		world: function (_: Array<RType>, options: HelperOptions) return switch (_) {
			case [Const(thing)]: options.data.adjective + " " + thing + (options.ctx.exclaim != null ? options.ctx.exclaim : "");
			case _: throw "Invalid context: " + _;
		} 
	}, Helpers.core());
}

